/***************************************************************************
 *   Copyright (C) 2007, 2009 by Marcelo Francis Maduar                    *
 *   maduar@gmail.com                                                      *
 *                                                                         *
 *   This file is part of OpenGamma.                                       *
 *                                                                         *
 *   OpenGamma is free software; you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   OpenGamma is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with OpenGamma. If not, see <http://www.gnu.org/licenses/>.     *
 ***************************************************************************/

#ifndef MAININTERFACE_H
#define MAININTERFACE_H

#include <QtGui/QMainWindow>
#ifdef Q_OS_LINUX
#include <QtGui/QMdiSubWindow>
#endif
#ifdef Q_OS_WIN32
#include <QtGui/QMdiSubwindow>
#endif
#include <QSignalMapper>

#include "ui_maininterface.h"
#include "spectrumform.h"
#include "spectrumformmodels.h"

class MainInterface : public QMainWindow
{
    Q_OBJECT
public:
    MainInterface(QWidget *parent = 0);
    ~MainInterface();
    QString getCurrentLang();
protected:
    void closeEvent(QCloseEvent *event);
private slots:
    void on_actionOpen_triggered();
    void on_action_Save_triggered();
    void on_actionSave_Plot_triggered();
    void cut();
    void copy();
    void paste();
    void on_actionAnaly_ze_triggered();
    void on_actionSet_F100D3P_triggered();
    void on_actionSet_XTRA_F100_triggered();
    void on_action_Exit_triggered();
    void on_action_File_locations_triggered();
    void on_actionLanguage_triggered();
    void on_action_about_triggered();
    void updateMenus();
    void updateWindowMenu();
    SpectrumForm *createMdiChild( const QString&, Counts *counts, SpectrumIO *io );
    void setActiveSubWindow(QWidget *window);
private:
    Ui::MainInterfaceClass *ui;
    QMdiArea *mdiArea;
    QSignalMapper *windowMapper;
    SpectrumForm *activeMdiChild();
    QMdiSubWindow *findMdiChild(const QString &fileName);
    void readSettings();
    void writeSettings();
    // default paths:
    QString spectraPath;
    QString analysisNuclibPath;
    QString calibNuclibPath;
    //
    void getSpecifiedDirectoryResults( QString Diretorio );
    QString m_lang;
};

#endif // MAININTERFACE_H
