#include "efficiencyplotterwidget.h"

//
// efficiency plotter:
//

EfficiencyPlotterWidget::EfficiencyPlotterWidget( QString &title, QWidget *parent )
  : PlotterWidget( title, true, 0.0, 0.01, parent )
{
    QCheckBox *ckbPlotEfficPoints = new QCheckBox("Points");
    QCheckBox *ckbPlotFit = new QCheckBox("Fit");

    QList<QCheckBox *> setsCheckBoxes;
    setsCheckBoxes <<
      ckbPlotEfficPoints << ckbPlotFit;
    addSetsCheckBoxes( setsCheckBoxes );

    connect( ckbPlotFit, SIGNAL(stateChanged(int)), this, SLOT( changed1(int)) );
    connect( ckbPlotEfficPoints, SIGNAL(stateChanged(int)), this, SLOT( changedErrBars(int)) );

    FunctionRange *fr1 = new FunctionRange();
    fr1->typeParam = FunctionRange::yParms;
    fr1->y = FunctionsDefinitions::poly;
    funcRngSet1->append( fr1 );
}

void EfficiencyPlotterWidget::plotBarsAndCurve( Doblst xlst, Doblst ylst, Doblst slst, Doblst parms )
{
    setEfficiencyErrBars( xlst, ylst, slst );
    plotScatterPlot( Qt::darkYellow, Qt::darkGreen, Qt::cyan, Qt::blue, 5 );
    funcRngSet1->at(0)->y = FunctionsDefinitions::polyLog;
    funcRngSet1->at(0)->parmsy = parms;
    funcRngSet1->at(0)->t0 = 30.0; //           the effic curve begins at 30 keV
    funcRngSet1->at(0)->t1 = xlst.max();

    plotCurveSet( crvSet1, funcRngSet1, Qt::magenta );
}

void EfficiencyPlotterWidget::setEfficiencyErrBars( Doblst x, Doblst y, Doblst s)
{
    setErrBarsAllEnabled( x, y, s );
}

void EfficiencyPlotterWidget::changed1(int state )
{
    if ( state == Qt::Checked )
        showCurveSet( crvSet1 );
    else
        hideCurveSet( crvSet1 );
}

void EfficiencyPlotterWidget::changedErrBars(int state )
{
    if ( state == Qt::Checked )
        errBarsQpu->show();
    else
        errBarsQpu->hide();
}
