# KickSorter

Open Source Debian Linux Based Gamma Spectroscopy Applicaiton.

Based on an initial fork of lrpingel's OpenGammaX: http://sourceforge.net/projects/opengammax/

Which in turn was forked from maduar's OpenGamma: http://sourceforge.net/projects/opengamma/

This project would not have been possible without these giants offering their shoulders to be stood upon. Full recognition is given to these guys and their excellent work.

This project aims to take this as a start point creating a solely Linux native Gamma Spectroscopy MCA/MCS tool targeted on the Debian distribution, with the hope of eventualy creating a binary package that can become a resident of the Debian standard repositories. Easing uptake, installation and useage.

## Directorys
* opengammax.0.1.1, lrpingels original copy of OpenGammaX on which this project is based.

## Notes:-
1. Clone repo and then setup autotools with "autoreconf --install"
2. Build with "./configure" then "make" and or "make install"